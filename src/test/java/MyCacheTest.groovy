import spock.lang.Specification

class MyCacheTest extends Specification {

    def "test LRU"() {
        given:
        MyCache cacheLRU = new MyCache(3, Strategy.LRU)
        cacheLRU.put("1", "VALUE1")
        cacheLRU.put("2", "VALUE2")
        cacheLRU.put("3", "VALUE3")
        cacheLRU.get("3")
        cacheLRU.get("1")
        cacheLRU.get("2")

        when:
        cacheLRU.put("4", "VALUE4")
        cacheLRU.put("2", "VALUE5")

        then:
        cacheLRU.get("3") == null
        cacheLRU.get("1") == "VALUE1"
        cacheLRU.get("4") == "VALUE4"
        cacheLRU.get("2") == "VALUE5"
    }

    def "test LFU"() {
        given:
        MyCache cacheLFU = new MyCache(3, Strategy.LFU)
        cacheLFU.put("1", "VALUE1")
        cacheLFU.put("2", "VALUE2")
        cacheLFU.put("3", "VALUE3")
        cacheLFU.get("2")
        cacheLFU.get("2")
        cacheLFU.get("1")
        cacheLFU.get("1")
        cacheLFU.get("3")

        when:
        cacheLFU.put("4", "VALUE4")
        cacheLFU.put("2", "VALUE5")

        then:
        cacheLFU.get("3") == null
        cacheLFU.get("1") == "VALUE1"
        cacheLFU.get("4") == "VALUE4"
        cacheLFU.get("2") == "VALUE5"
    }
}
