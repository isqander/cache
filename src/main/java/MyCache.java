import java.util.ArrayDeque;
import java.util.LinkedHashMap;
import java.util.Map;

public class MyCache<K, T> {
    int maxSize;
    Strategy strategy;
    ArrayDeque<K> keys = new ArrayDeque<>();
    LinkedHashMap<K, Element> storage = new LinkedHashMap();

    public MyCache(int maxSize, Strategy strategy) {
        this.maxSize = maxSize;
        this.strategy = strategy;
    }

    public T get(K key) {
        Element element = storage.get(key);
        if (element == null){
            return null;
        }
        if (strategy == Strategy.LFU) {
            element.incrementFrequency();
        } else {
            keys.remove(key);
            keys.addFirst(key);
        }
        return element.getValue();
    }

    public T put(K key, T value) {
        if (storage.size() >= maxSize && !storage.containsKey(key)) {
            deleteElement(key);
        }
        Element oldNode = storage.put(key, new Element(value));
        if (oldNode == null) {
            return null;
        }
        return oldNode.getValue();
    }

    private void deleteElement(K key) {
        K keyToRemove = null;
        if (strategy == Strategy.LFU) {
            int minFrequency = Integer.MAX_VALUE;
            for (Map.Entry<K, Element> entry : storage.entrySet()) {
                if (minFrequency >= entry.getValue().getFrequency()) {
                    minFrequency = entry.getValue().getFrequency();
                    keyToRemove = entry.getKey();
                }
            }
        } else {
            keyToRemove = keys.getLast();
        }
        storage.remove(keyToRemove);
    }

    class Element {
        private T value;
        private int frequency;

        public Element(T value) {
            this.value = value;
            this.frequency = 0;
        }

        public T getValue() {
            return value;
        }

        public int getFrequency() {
            return frequency;
        }

        public void incrementFrequency() {
            frequency++;
        }
    }
}





